################################################################################
#
# pssensor
#
################################################################################

PSSENSOR_SITE=$(TOPDIR)/../pssensor
#PSSENSOR_SITE_METHOD=git
#PSSENSOR_GIT_SUBMODULES=yes
PSSENSOR_OVERRIDE_SRCDIR = $(PSSENSOR_SITE)
PSSENSOR_OVERRIDE_SRCDIR_RSYNC_EXCLUSIONS = --exclude `cd $(PSSENSOR_SITE) && cat .gitignore | tr "\n" ";" | sed 's/\;/ \-\-exclude /g'` *.log
PSSENSOR_DEPENDENCIES = linux openssl

define PSSENSOR_CONFIGURE_CMDS
	mkdir -p $(TARGET_DIR)/usr/local/bro
endef

define PSSENSOR_BUILD_CMDS
	@cp -rf $(PSSENSOR_SITE)/.git $(@D)/.git
	@cp -f $(PSSENSOR_SITE)/version.conf $(@D)/.
	$(MAKE) -C $(@D) all test packages
endef

define PSSENSOR_CLEAN_CMDS
	$(MAKE) -C $(@D) distclean
endef

define PSSENSOR_INSTALL_INIT_SYSV
	$(INSTALL) -d -m 0777 $(TARGET_DIR)/var/www
endef

$(eval $(generic-package))
