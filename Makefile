#buildroot Makefile
BR_TOP_DIR=$(PWD)
MAKEFLAGS+=--no-print-directory
#MAKEFLAGS+=--silent

PSSENSOR_BRANCH=develop

CONFIG_FILES=$(BR_TOP_DIR)/buildroot/.config 
BOARD_DEFCONFIG=pssensor_x86_64_bios_defconfig
include $(BR_TOP_DIR)/externals/configs/$(BOARD_DEFCONFIG)

BUILD_OUTPUT_DIR=$(BR_TOP_DIR)/buildroot/output
.default: all
all: $(CONFIG_FILES) $(BR_TOP_DIR)/pssensor/README.md
	make -C $(BR_TOP_DIR)/buildroot all 2>&1 | tee logmake_`date +%Y%m%d_%H%M%S`.log 
	
clean:
	make -C $(BR_TOP_DIR)/buildroot clean
	rm -rf $(BR_TOP_DIR)/logmake*.log $(PROCESSED_DTS_FILE)

sync2build4:
	@rsync -ru * chinshaw@build5.mixmode.ai:~/sensor_buildroot/. --exclude .git --exclude buildroot

%config:
	make -C $(BR_TOP_DIR)/buildroot $@

%-rebuild: $(CONFIG_FILES)
	make -C $(BR_TOP_DIR)/buildroot $@ all 2>&1 | tee logmake_`date +%Y%m%d_%H%M%S`.log

$(BR_TOP_DIR)/pssensor/README.md:
	git clone git@bitbucket.org:packetsled/pssensor.git pssensor -b $(PSSENSOR_BRANCH)

$(BR_TOP_DIR)/buildroot/.config: $(BR_TOP_DIR)/buildroot/README $(BR_TOP_DIR)/externals/configs/$(BOARD_DEFCONFIG)
	make -C $(BR_TOP_DIR)/buildroot BR2_EXTERNAL=$(BR_TOP_DIR)/externals BR_DEFCONFIG=$(BR_TOP_DIR)/externals/configs/$(BOARD_DEFCONFIG) $(BOARD_DEFCONFIG)

